import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Header from "./Header";
import PizzaList from "./PizzaList";
import Order from './Order';
import Basket from "./Basket";
import store from './store';
import { asyncAction } from './actions';


/* Main Component */
class Main extends Component {
    constructor(props) {
        super(props);
        window['main'] = this;
        //Initialize the state in the constructor
        this.state = {
            render: false,
            store: store.getState().mainState
        };
        store.subscribe(() => {
            if (this.state.store.loaded) {
                this.setState({render: true})
            }
        })

    }

    componentDidMount() {
        store.dispatch(asyncAction('getPizza', {dispatch: 'getPizza'}))
    }

    render() {
        return (
         <div className={'mb-0'}>
                <Header backBtn={this.state.store.activePage !== "Home"} mainstore={store} storePizzaList={store.getState().pizzaListState} basket = {store.getState().basketState} />
              <div className={'container'}>
                    {this.state.store.activePage === "Basket" ? <Basket  mainstore={store} store={store.getState().basketState}   /> : null}
                    {this.state.store.activePage === "Home"  && this.state.store.loaded ? <PizzaList mainstore={store} store={store.getState().pizzaListState} basket = {store.getState().basketState} /> : null}
                    {this.state.store.activePage === "Order" ? <Order mainstore={store} store={store.getState().orderState} />: null}
              </div>
         </div>
        );

    }
}
 export default Main;
if (document.getElementById('container')) {
    ReactDOM.render(<Main />, document.getElementById('container'));
}
