import React, { Component } from 'react';
import ReactDOM from 'react-dom';

class Delivery extends Component {
    order;
    orderTotal;
    selected = 0;
    constructor(props) {
        super(props);
        //Initialize the state in the constructor
        window['delivery'] = this;
        this.order =  this.props.mainstore.getState().orderState;
        this.orderTotal = parseFloat(this.order.totalPrice);
    }

    updateTotal() {
        const cost = this.order.deliveryItems[this.selected].cost;
        const totalSum = (cost + this.orderTotal).toFixed(2)
        this.props.mainstore.dispatch({type: 'updateTotalOrder', data: totalSum })
    }

    chooseDelivery(ev) {
        this.selected = parseInt(ev.target.value);
        this.updateTotal();
    }
    componentDidMount() {
        this.mainSubscriber = this.props.mainstore.subscribe(() => {
            if (this.props.store.loaded) {
                console.log('99render')
                this.setState({render: true});
            }
        });
        this.updateTotal();
    }


    componentWillUnmount() {
        this.props.mainstore.dispatch({type: 'updateTotalOrder', data: this.orderTotal, sum: false})
    }


    getDeliveryOptions() {
        if (this.props.delivery.length > 0) {
            return this.props.delivery.map((el, i) => {
                const deliveryType = el.type === 1 ? 'City' : 'Region';
                return (
                    <div className={'col-12'}>
                        <input onChange={(ev)=>  {this.chooseDelivery(ev)} } checked={i === this.selected} type ='radio' name={'delivery'} value = {i}/>
                        <span className={'ml-2 delvery-desc'}>{deliveryType} delivery/ Cost: {el.cost}</span>
                    </div>
                )
            })
        }
    }

    render() {
        return (

            <div className={'col-12 row m-0 delivery align-items-center mb-3'}>
                <p>Choose your delivery type:</p>
                {this.getDeliveryOptions()}
            </div>
        );

    }
}
 export default Delivery;

