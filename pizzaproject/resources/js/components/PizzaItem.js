import React, { Component } from 'react';
import ReactDOM from 'react-dom';

/* Main Component */
class PizzaItem extends Component {

    constructor({item}) {
        super();
        //Initialize the state in the constructor

    }



    /*componentDidMount() is a lifecycle method
     * that gets called after the component is rendered
     */
    componentDidMount() {
        console.log('****', this.props)

    }



    render() {
        return (
            <div className={'row pizza-item-wrap col-12 m-0 col-lg-8'}>
                <div className={'pizza-item col-12 '}>
                    <div className={'row col-12 justify-content-end'}>
                        <p onClick={() => {this.props.isShow()}} >Закрыть</p>
                    </div>
                    <div className={'pizza-image col-12 p-0'} style={{background:`url(/image/${this.props.item.img}) center/cover no-repeat`}}></div>
                    <div className={'p-2'}>
                        <h3>{this.props.item.title}</h3>
                        <p>{this.props.item.full_desc}</p>
                    </div>

                </div>
            </div>
        )
    }
}
export default PizzaItem;

