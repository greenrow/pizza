import React, { Component } from 'react';
import Delivery from './Delivery';
import { asyncAction } from './actions';

class Order extends Component {
    mainSubscriber;
    constructor(props) {
        super(props);
        //Initialize the state in the constructor
        window['order'] = this;
        this.state = {
            render: false
        }
    }

    componentDidMount() {
        const basketStore = this.props.mainstore.getState().basketState;
        this.mainSubscriber = this.props.mainstore.subscribe(() => {
            if (this.props.store.loaded) {
                console.log('99render')
                this.setState({render: true});
            }
        });
        this.props.mainstore.dispatch({type: 'initOrder', data: basketStore.items, total: basketStore.totalPrice})
        this.props.mainstore.dispatch(asyncAction('getDelivery', {dispatch: 'getDelivery'}))
    }

    componentWillUnmount() {
        this.mainSubscriber();
    }

    updateDelivery() {
        this.props.mainstore.dispatch({type: 'updateDelivery'})
    }
    renderItemsInfo() {
        return this.props.store.orderItems.map((el, i) => {
            return (
                <div  key={i}>
                    <div className={'order-item-title'}>
                        {el.fullTitle }
                    </div>
                    <div>{el.count}</div>
                    <div>{el.totalPrice}</div>
                    <div>{el.totalCount}</div>
                    <div>{el.currencyTypes[el.selectedtypes]}</div>

                </div>

            )
        });
    }
    render() {
        return (
            <div className={'col-12 row m-0  order align-items-center mb-3'}>
                <h4 className={'text-center my-4'}>Order info</h4>
                <div className={'order-items col-12 p-0 mb-3'}>
                    <div className={'order-header'}>
                        <div>Name</div>
                        <div>Count</div>
                        <div>Price for 1 item</div>
                        <div>Sum price</div>
                        <div>Currency</div>

                    </div>
                    {this.renderItemsInfo()}
                </div>
                <div>
                    <label className={'mr-2'} htmlFor={'delivery'}>Delvery</label>
                    <input id={'delivery'} type={'checkbox'}  checked={this.props.store.delivery} onChange={() => {this.updateDelivery()}}/>
                </div>
                {this.props.store.delivery ? <Delivery mainstore={this.props.mainstore} delivery={this.props.store.deliveryItems}/> : null}
                <div className={'col-12'}>
                    <h4>Total: {this.props.store.totalPrice}</h4>
                </div>
            </div>
        );

    }
}
 export default Order;

