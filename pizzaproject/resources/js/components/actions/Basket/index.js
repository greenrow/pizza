import { ADD_ITEM_BASKET, REMOVE_ITEM_BASKET, TOTAL_SUM, INIT} from './constants';

export function addPizzaBasket(data) {
    return {type: ADD_ITEM_BASKET, data: data}
}
export function removePizzaBasket(data) {
    return {type: REMOVE_ITEM_BASKET, data: data}
}
export function getTotal() {
    return {type: TOTAL_SUM}
}
export function init() {
    return {type: INIT}
}

