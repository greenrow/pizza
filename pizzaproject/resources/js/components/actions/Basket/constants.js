export const ADD_ITEM_BASKET = 'basket_add_item';
export const REMOVE_ITEM_BASKET = 'basket_remove_item';
export const TOTAL_SUM = 'setTotalBasket';
export const INIT = 'init_basket';
