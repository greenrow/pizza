import React, { Component } from 'react';
import * as actions from './actions/Basket/';

class Basket extends Component {
    mainSubscriber;
    constructor(props) {
        super(props);
        //Initialize the state in the constructor
        window['basket'] = this;
        this.state = {
            render: true,
        }
    }

    getTotal() {
        this.props.mainstore.dispatch(actions.getTotal())
    }

    componentWillMount() {
        this.mainSubscriber = this.props.mainstore.subscribe(() => {
            if (this.props.store.loaded) {
                this.setState({render: true})
            }
        });
        if (this.props.store.items.length> 0) {
            this.props.mainstore.dispatch(actions.init())
        }

    }

    componentWillUnmount() {
        this.mainSubscriber();
    }

    incCount(item) {
        item.count += 1;
        item.totalCount = ( item.totalPrice * item.count);
        this.getTotal();
    }

    descCount(item) {
        if(item.count > 1) {
            item.count -= 1;
            item.totalCount = (item.totalCount  -  item.totalPrice);
            this.getTotal();
        }
    }

    changeType(ev) {
        const selected = parseInt(ev.target.value);
        this.props.mainstore.dispatch({type:'changeType', value: selected})
    }


    renderCurrencyTypes() {
        return this.props.store.currencyArr.map((el, i) => {
            let selectInputItem =
               <input onChange={($event) => {this.changeType( $event)}} className={'ml-2'} name = {'currency'} type = 'radio' checked={this.props.store.currencyType === i} value={i}/>;
            return  (
                <div className={'ml-2'}>
                    <span>{el}</span>
                    {selectInputItem}
                </div>
            )
        })
    }
    removeItem(item) {
        this.props.mainstore.dispatch(actions.removePizzaBasket(item));
    }

    renderBasketItems() {
        return this.props.store.items.map((el, i) => {
            return (
                <div className={'row justify-content-start col-12 col-sm- 10 col-md-8 my-3 m-0 p-0  '} key={el.id + el.selected} >
                    <div className={'pizza-image-basket col-4'} style={{background:`url(/image/${el.img}) center/cover no-repeat`}}></div>

                    <div className={'row m-0 align-items-center justify-content-center'}>
                        {el.fullTitle}
                    </div>
                    <div className={'justify-content-start pr=3'}>
                        <div>
                            Count <span className={'font-weight-bold'} >{el.count}</span>
                        </div>
                        <div className={' ml-2 count-btn count-btn-plus'} onClick = {()=> {this.incCount(el)}} >
                        </div>
                        <div className={'ml-2 count-btn count-btn-minus'} onClick = {()=> {this.descCount(el)}} >

                        </div>
                    </div>
                    <div>
                        Price: <span className={'pl-1 font-weight-bold'}>{el.price+'/'+el.priceOther}</span>
                    </div>

                    <div className={' total-count col-12 row m-0    '}>
                        <div>
                            Total: <span className={'pl-1 font-weight-bold'}>{el.totalCount.toFixed(2)}</span>
                        </div>

                        <div onClick={() => {this.removeItem(el)}} className={'remove-item p-1 ml-auto'}>remove</div>
                    </div>
                </div>


            )
        })
    }

    showItems() {
        if (this.props.store.items.length > 0) {
            return (
                <div>
                    <div className={'basket-items justify-content-center flex-wrap d-flex'}>
                        {this.renderBasketItems()}
                    </div>
                    <div className={'d-flex justify-content-center'}>
                        <div className = {' d-flex basket-footer mt-4'}>
                            <div className={ 'mr-5'}>
                                <div className = {'mb-3'}>
                                    Currency: <div className={'d-flex'}>{this.renderCurrencyTypes()} </div>
                                </div>
                                <div>
                                    Total: <h4>{this.props.store.totalPrice}</h4>
                                </div>
                            </div>

                            <div className={' d-flex align-items-center justify-content-center'}>
                                <button type='button' onClick={() => {this.props.mainstore.dispatch({type: 'goToPage', page: 'Order'})}} className={'btn btn-success'}>Make order</button>
                            </div>
                        </div>

                    </div>

                </div>
            )
        } else {
            return (
                <div className={'text-center m-3'}>
                    No items in Basket. Please add pizza..
                </div>
            )
        }
    }

    render() {
        /* Some css code has been removed for brevity */
        return (
            <div className={'basket-page'}>
                {this.showItems()}
            </div>
        );
    }
}
 export default Basket;

