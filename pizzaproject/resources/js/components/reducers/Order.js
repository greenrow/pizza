const MAIN_STATE = {
    orderItems: [],
    delivery: false,
    deliveryItems: [],
    deliverySelected: 0,
    totalPrice: null,
    loaded: false,
    currencyTypes: null
}

function updateTotal(state, action) {
    if (action.sum) {
        const intTotal = parseFloat(state.totalPrice)
        state.totalPrice = (intTotal + action.data).toFixed(2);
    } else {
        state.totalPrice = action.data;
    }
}
function reducerOrder(state = MAIN_STATE, action) {

    switch(action.type) {
        case 'initOrder': {
            state.orderItems = action.data;
            state.totalPrice = action.total;
            state.currencyTypes = action.data
            state.loaded = true;
            return state;
        }
        case 'getDelivery': {
            state.deliveryItems = action.data;
            return state;
        }

        case 'updateDelivery': {
            if (action.data) {
                state.delivery = false
            } else {
                state.delivery = !state.delivery;
            }

            return state;
        }
        case 'updateTotalOrder': {
            state.totalPrice = action.data;
            return state;
        }

        default : return state;
    }
}
export default reducerOrder
