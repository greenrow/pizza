import {addToArray} from "../actions";


const MAIN_STATE = {
    pizzaTypes: null,
    items: [],
    count: 0,
    pizzaItems: [],
    activePage : 'Home',
    sharedObj: {},
    refresh: false,
    loaded: false,
    id: Math.random(1,100),
    pages: ['Home','Basket', 'Order'],
    pageStep: 0
}


function reducerMain(state = MAIN_STATE, action) {
    switch(action.type) {

        case 'getPizza' : {
            state.pizzaItems = action.data.items;
            state.pizzaTypes = action.data.types;
            state.loaded = true;
            return state;
        }
        case 'removeItemToBasket' : {
            state.basketItems = state.basketItems.filter((el) => {
                return el.id + el.selected !== action.items.selected + action.items.id;
            });
            return state;
        }
        case 'goToPage' : {
            state.pageStep++;
            state.activePage = action.page;
            return state;
        }
        case 'backToPage' : {
            state.pageStep--;
            state.activePage = state.pages[state.pageStep];
            return state;
        }
        case 'incCount' : {

            break;
        }
        default : return state;
    }
}
export default reducerMain
