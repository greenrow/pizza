
const MAIN_STATE = {
    items: [],
    itemChoosen: false,
    showItem: false,
    showPizzaType: true,
    pizzaTypes: null,
    typeSelected: 0,
    loaded: false
}

function updateData(state) {
    state.items.forEach((el) => {
        el.basketIdArr = [];
        el.pizza_price.pizza
    })
}
function addTypes(state) {
    const allItem = { name: 'All'};
    state.unshift(allItem);
}

function filterStateByType(state) {
    if (state.typeSelected !== 0) {
        state.items = state.itemsCopy.filter((el) => {
            return el.pizza_type[0].id === state.typeSelected;
        })
    } else {
        state.items = state.itemsCopy;
    }
}

function reducerPizzaList(state = MAIN_STATE, action) {

    switch(action.type) {

        case 'initPizzaList': {
            state.items = state.itemsCopy = action.data.pizzaItems;
            state.pizzaTypes = action.data.pizzaTypes;
            updateData(state);
            addTypes( state.pizzaTypes);
            state.loaded = true;
            state.showPizzaType = true;
            return state;
        }

        case 'removeFromBasket': {
            state.basketItems.splice(action.index, 1);
            updateTotal(state);
            return state;
        }

        case  'filterByType': {
                state.typeSelected = action.data;
                filterStateByType(state);
                return state;
        }

        case  'hidePizzaTypes': {
            state.showPizzaType = false;
            return state;
        }


        default : return state;
    }
}
export default reducerPizzaList
