import { ADD_ITEM_BASKET, REMOVE_ITEM_BASKET, TOTAL_SUM, INIT } from '../actions/Basket/constants';

const MAIN_STATE = {
    count: 0,
    items: [],
    currencyType: 0,
    totalPrice: 0,
    loaded: false,
    initDelete: false,
    currencyArr: null
}
function updateItems(el) {
    if (!el.totalCount) {
        el.totalPrice  = el.totalCount = parseFloat(el.price);
    }
    const sizeText = el.selected === 0 ?  '30': '50';
    el.fullTitle = `${el.title } -  ${sizeText} `;
    return el;
}

function updateTotal(state) {
    if (state.items.length > 0) {
        state.items.forEach((el) => {
            el.totalPrice  = state.currencyType === 0 ? parseFloat(el.price) : parseFloat(el.priceOther);
            el.totalCount = (el.totalPrice * el.count);
        });

        state.totalPrice = state.items.reduce((accum, next) => {
            return accum + next.totalCount;
        }, 0);
        state.totalPrice = state.totalPrice.toFixed(2);
    }
}

function reducerBasket(state = MAIN_STATE, action) {
    const type = state.currencyType;
    switch(action.type) {
        case INIT: {
            state.items.forEach((el) => {
                updateItems(el);
            });
            updateTotal(state);
            state.currencyArr = state.items[0].currencyTypes;
            state.loaded = true;
            return state;
        }
        case 'changeType': {
            state.currencyType = action.value;
            updateTotal(state);
            return state;
        }
        case ADD_ITEM_BASKET: {
            state.items.push(action.data);
            const basketIndex = state.items.length;
            action.data.basketIndex = basketIndex;
            return state;
        }
        case REMOVE_ITEM_BASKET: {
            const item = action.data;
            // remove value from arr link
            const indexToDel = item.basketIdArr.indexOf(item.selected);
            item.basketIdArr.splice(indexToDel, 1);
            // remove item
            state.items.splice(item.index, 1);
            updateTotal(state);
            return state;
        }
        case TOTAL_SUM: {
            updateTotal(state);
            return state;
        }
        case 'unmountBasket' : {
            state.loaded = false;
            return null;
        }
        default : return state;
    }
}
export default reducerBasket
