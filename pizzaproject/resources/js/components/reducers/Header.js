import {addToArray} from "../actions";

const MAIN_STATE = {
    items: [],
    count: 0,
    basketItems: [],
    pizzaItems: [],
    activePage : "Home",
    sharedObj: {},
    refresh: false
}
function reducerHeader(state = MAIN_STATE, action) {

    switch(action.type) {
        default : return state;
    }
}
export default reducerHeader
