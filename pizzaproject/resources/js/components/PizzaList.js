import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PizzaItem from './PizzaItem';
import Header from "./Header";
import * as actions from './actions/Basket/';
import Select from 'react-select';
import { selectStyles } from './ui/selectStyles';



/* Main Component */
class PizzaList extends Component {
    mainSubscriber;
    constructor(props) {

        console.log('**h', Header)
        super(props);
        window['pizzalist'] = this;
        //Initialize the state in the constructor
        this.state = {
            render: false
        }
    }

    componentWillMount() {

        this.mainSubscriber = this.props.mainstore.subscribe((type) => {
            if (this.props.store.loaded) {
                this.setState({render: true})
            }
        });

        if (!this.props.store.loaded) {
            this.props.mainstore.dispatch({type: 'initPizzaList', data: this.props.mainstore.getState().mainState})
        }
    }

    componentWillUnmount() {
        this.mainSubscriber();
    }
    changeShow() {
        this.setState(state => ({ showItem: !state.showItem }));
    }
    addPizza(e, item) {
        item.basketIdArr.push(item.selected);
        e.stopPropagation();
        const payType = item.pizza_price[0].price_rate[0];
        if (payType.type === 1) {
             item.currencyTypes = payType.payment_type[0].data.split('/');
        }
        item.count = 1;
        console.log('add2', actions.addPizzaBasket({...item}))
        this.props.mainstore.dispatch(actions.addPizzaBasket({...item}));
    }

    changeSelect(e, item, index) {
        console.log('321',e )
        item.selected = parseInt(e.value);
        this.setState({render: true});

    }

    removePizza(e, item, index) {
        e.stopPropagation();
        this.props.mainstore.dispatch(actions.removePizzaBasket(item));
    }

    changePriceData(item, index = null) {
        const priceObj = item.pizza_price[item.selected];
        const priceCost  = priceObj.price_rate[0]
        item.price = priceObj.price + 'eur';
        item.priceOther = (priceObj.price * priceCost.value).toFixed(2) + priceCost.currency;
        return  <div className={'col-12'}><p>Price: {item.price+'/'+item.priceOther}</p></div>
    }

    changePizzaData(item) {
        const selectObj = [];

         item.pizza_price.map((el, i) => {
                const pizza_size_text = el.pizza_size === 1 ? '30' : (el.pizza_size === 2 ? '50': '60')
                 selectObj.push({value: i, label: pizza_size_text})

        });

        return (
            <Select  theme={(theme) => ({
                ...theme,
                borderRadius: 10,
                colors: {
                    ...theme.colors,
                    primary: 'grey',
                    primary50: '#492a5',
                     }
            })}
                styles = {selectStyles}
                value = {selectObj[item.selected]}
                isSearchable = {false}
                placeholder={false}
                onChange={(event) => {this.changeSelect(event,item)}}
                options={selectObj}
            />
        );
    }

    checkBtnItems() {
        return this.props.store.items.find((el) => {
            return el.selected === parseInt(item.selected);
        })
    }
    renderItems() {
        return this.props.store.items.map((item, id) => {
            item.selectedtypes = 0;
            item.count = 1;
            item.rate = item.pizza_price[item.selectedtypes].price_rate;
            if (!item.selected) { item.selected = 0 }
            let btnType;
            if (item.basketIdArr.includes(item.selected)) {
                btnType = <button className={'btn '} onClick={(event) => {this.removePizza(event, item, id)}}>Убрать из корзины</button>
            } else {
                btnType = <button className={'btn '} onClick={(event) => {this.addPizza(event, item)}}>Добавить в корзину</button>;
            }
            return (
                <div className={'col-md-4 col-sm-6 col-12 p-1'}>
                    <div  className={'pizza-item-list h-100 p-0'} key={item.id} >
                        <div onClick={() => this.handleClick(item)} className={'pizza-image col-12'} style={{background:`url(/image/${item.img}) center/cover no-repeat`}}></div>
                        <div className={'p-2 footer-item-block '}>
                            <div>
                                <h3>{item.title}</h3>
                                <p>{item.description}</p>
                            </div>
                            <div className={'p-0 row m-0 align-items-center'}>
                                    <label className={'m-0 mr-2'}>size:</label>
                                    {this.changePizzaData(item)}
                            </div>
                            {this.changePriceData(item)}
                            <div className={' d-flex justify-content-center'}>
                                {btnType}
                            </div>
                        </div>
                    </div>

                 </div>
            );
        })
    }

    handleClick(product) {
        this.setState({itemChoosen:product, showItem : true});
    }
    render() {
        return (
            <div>
            <div className={'pizza-list-wrap row m-0 justify-content-center '}>

                    { this.renderItems() }

            </div>
                {this.state.showItem ? <PizzaItem item={this.state.itemChoosen} isShow={this.changeShow.bind(this)} />  : null}
            </div>
        );

    }
}
export default PizzaList;
if (document.getElementById('pizzalist')) {
    ReactDOM.render(<PizzaList />, document.getElementById('pizzalist'));
}
