import React, { Component } from 'react';
import ReactDOM from 'react-dom';
/* Main Component */
class Header extends Component {

    constructor(props) {
        super(props);
        window['header'] = this;

        this.state = {
            render: false,
            showTypes: false,
            mainState: this.props.mainstore.getState().mainState
        }
    }
    componentDidMount() {
        this.props.mainstore.subscribe((val)=> {
            this.setState({render: true});
            if (this.props.storePizzaList.loaded) {
                this.setState({showTypes: this.state.mainState.activePage === 'Home'})
            }

        })
    }

    renderBackBtn() {
       const tmpl = <div onClick={() => { this.props.mainstore.dispatch({type: 'backToPage'})}}  className={'backBtn mr-4 '}>
            <span >Back</span>
        </div>
        return tmpl;
    }

    filterByType(index) {
        this.props.mainstore.dispatch({type: 'filterByType', data: index});
    }


    showPizzaTypes() {
        const pizzaList = this.props.storePizzaList;
        const types =  pizzaList.pizzaTypes;
        const renderTypes = types.map((el, i) => {
            return (
                <div onClick={() => {this.filterByType(i)}} className={pizzaList.typeSelected === i? 'active' : ''}>
                    {el.name}
                </div>
            )
        })

        return (
            <div className={'pizza-type-wrap'}>
                <div className={'d-flex pizza-type'}>
                    {renderTypes}
                </div>
            </div>
        )



    }

    render() {
        /* Some css code has been removed for brevity */
        return (
            <div className={'content-header'}>
                <div className={'col-12 d-flex header align-items-center '}>
                    <div className={this.props.backBtn ? 'col-4 col-sm-8' : 'col-8 col-sm-10'}>
                        pizzalist
                    </div>
                    <div className={'ml-auto d-flex'}>
                        {this.props.backBtn ? this.renderBackBtn(): null}
                        <div onClick={() => { this.props.mainstore.dispatch({type: 'goToPage', page: 'Basket'})}} className={'  userbusket mr-4 d-flex justify-content-end '}>
                            <span className={'mr-2'}>Basket</span><span> {this.props.basket.items.length}</span>
                        </div>
                    </div>
                </div>

                {this.state.showTypes ? this.showPizzaTypes() : null}

            </div>

        );

    }
}

export default Header;
if (document.getElementById('count')) {
    ReactDOM.render(<Header />, document.getElementById('count'));
}
