export const selectStyles =   {
    valueContainer: () => ({
        // none of react-select's styles are passed to <Control />
        width: 60,
        height:30,
        paddingLeft: 5,
        fontSize: '0.9em'
    }),

    control: (provide, state) => ({
        ...provide,
        padding: 0,
        height: 30,
        minHeight: 30,
        boxShadow: 'none',
        '&:hover': {
            borderColor: 'grey',
        }
    }),
    indicatorsContainer: (provide) => ({
        ...provide,
        padding: 0,
        height: 30,
        minHeight: 30
    }),
    option: (provide, state) => {
        console.log('state4', state)
        return {

            ...provide,
            height: 30,
            minHeight: 30,
            color:  state.isFocused ? '#fff' : 'inherit',
            backgroundColor: state.isFocused ? '#765d77' : state.isActive ? 'white' : 'inherit'
        }}
}
