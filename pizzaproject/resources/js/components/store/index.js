import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk'
import reducerHeader from "../reducers/Header";
import reducerMain from "../reducers/Main";
import reducerPizzaList from '../reducers/PizzaList'
import reducerBasket from "../reducers/Basket";
import reducerOrder from '../reducers/Order'

const mainReducer = combineReducers({
    headerState: reducerHeader,
    mainState: reducerMain,
    pizzaListState: reducerPizzaList,
    basketState: reducerBasket,
    orderState: reducerOrder
  });
const store = createStore(mainReducer, applyMiddleware(thunk));
store.id =  Math.random(1,100);
console.log('store', store)
export default store;

