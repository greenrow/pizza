<?php

namespace App\Http\Controllers;
use App\Pizza;

use App\Delivery;
use App\PizzaPrice;
use App\PizzaType;
use Illuminate\Http\Request;

class PizzaController extends Controller
{
    public function index() {
         $pizza_item = Pizza::with(['pizza_price.price_rate.payment_type', 'pizza_type'])->get();
         $pizza_type = PizzaType::all();
         $rez = array_merge(['items' =>$pizza_item],['types'=>$pizza_type] );

        return response($rez,200,['accept'=>'application/json']);
    }
    public function delivery() {
        $delivery = Delivery::all();

        return response($delivery ,200,['accept'=>'application/json']);
    }
}
