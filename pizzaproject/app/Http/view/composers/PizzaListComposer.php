<?php
namespace App\Http\View\Composers;
use App\DbData\PizzaList;
use Illuminate\View\View;

class PizzaListComposer
{
    protected $pizzaList;

    public function __construct(PizzaList $pizza)

    {

        $this->pizzaList = $pizza->pizza;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('pizza', $this->pizzaList);
    }
}
