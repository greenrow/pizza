<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PizzaPrice extends Model
{
    protected $table = 'pizza_price';
    public $timestamps = false;
    public function price_rate()
    {
        return $this->hasMany('App\PriceRate','id','payment_type');
    }
    public function pizza_item()
    {
        return $this->hasMany('App\Pizza','id','pizza_id');
    }


}
