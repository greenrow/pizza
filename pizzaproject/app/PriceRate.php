<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PriceRate extends Model
{
    protected $table = 'rate';
    public $timestamps = false;

    public function payment_type()
    {
        return $this->hasMany('App\PaymentType','type','type');
    }
}
