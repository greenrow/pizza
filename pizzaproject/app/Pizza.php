<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pizza extends Model
{
    protected $table = 'pizza';
    public $timestamps = false;
    protected $fillable = ['title', 'description', 'active', 'full-desc', 'image'];

    public function pizza_price()
    {
        return $this->hasMany('App\PizzaPrice','pizza_id');
    }
    public function pizza_type()
    {
        return $this->hasMany('App\PizzaType','id','type');
    }

    public function getPizza() {
        $pizza =  $this->pizza_price;
        $types = $this->pizza_type;
        return $pizza->merge($types);
    }
}
