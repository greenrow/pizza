<?php

use Illuminate\Database\Seeder;
use App\Pizza;
class PizzasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        // Create 50 product records
        for ($i = 0; $i < 50; $i++) {
            Pizza::create([
                'name' => $faker->title,
                'description' => $faker->text(10),
                'active' => $faker->boolean(50)
            ]);
        }
    }
}
