<?php

use Illuminate\Database\Seeder;
use App\Pizza;
class PizzaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        // Create 50 product records
        for ($i = 0; $i < 10; $i++) {
            Pizza::create([
                'title' => 'Гавайская',
                'description' => 'Гавайская описание',
                'active' => $faker->boolean(10)
            ]);
        }
    }
}
